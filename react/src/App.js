import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Alarm from './components/Alarm';
import Clock from './components/Clock';
import Link from './components/Link';
import linkData from './data/links.json';
import './App.css';

const App = () =>
  <MuiThemeProvider>
    <div className="App">
      <Clock />
      <Alarms />
      <Links />
    </div>
  </MuiThemeProvider>;

const Alarms = () =>
  <div className="Alarms">
    <Alarm
      hour="7"
      minute="0"
      defaultToggled={true}
    />
    <Alarm
      hour="8"
      minute="0"
      defaultToggled={false}
    />
  </div>;

const Links = () =>
  <div className="Links">
    {linkData.map((x, key) => <Link key={key} label={x.label} href={x.href} />)}
  </div>;

export default App;

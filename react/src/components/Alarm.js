import React from 'react';
import Toggle from 'material-ui/Toggle';
import './Alarm.css';

const doit = () => (
  <audio
    id="alarmAudio"
    src="http://localhost/alarm.mp3"
    preload="auto"
    loop="true"
    autoPlay
  />
);

class Alarm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
    this.time = {
      hour: props.hour ? Number(props.hour) : 0,
      minute: props.minute ? Number(props.minute) : 0
    };
  }

  componentWillMount = () => {
    if (this.props.defaultToggled) {
      this.enableAlarm();
    }
  };

  componentWillUnmount = () => this.disableAlarm();

  enableAlarm = () => this.interval = setInterval(this.soundAlarm, 1000);
  disableAlarm = () => {
    this.setState(() => {
      return {
        active: false
      };
    });
    clearInterval(this.interval);
  };
  handleToggleChange = (e, enable) => {
    enable ? this.enableAlarm() : this.disableAlarm();
  };

  timeAsString = () =>
    `${this.time.hour
      .toString()
      .padStart(2, '0')}:${this.time.minute.toString().padStart(2, '0')}`;

  soundAlarm = () => {
    const now = new Date();
    const onTheHour = now.getHours() === this.time.hour;
    const onTheMinute = now.getMinutes() === this.time.minute;
    if (onTheHour && onTheMinute) {
      if (!this.state.active) {
        this.setState(() => {
          return { active: true };
        });
      }
    } else {
      this.setState(() => {
        return { active: false };
      });
    }
  };

  render = () => {
    const sound = this.state.active ? doit() : '';
    return (
      <div className="Alarm">
        <Toggle
          onToggle={this.handleToggleChange}
          defaultToggled={this.props.defaultToggled}
          label={this.timeAsString()}
          className="toggle"
          style={{
            maxWidth: '50px'
          }}
        />
        {sound}
      </div>
    );
  };
}

export default Alarm;

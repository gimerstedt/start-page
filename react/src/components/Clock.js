import React, { Component } from 'react';
import './Clock.css';

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      h: '',
      m: '',
      s: ''
    };
  }

  componentWillMount = () => {
    this.updateTime();
    this.interval = setInterval(this.updateTime, 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.interval);
  };

  updateTime = () => {
    const now = new Date();
    this.setState((state, props) => {
      return {
        h: this.padNr(now.getHours()),
        m: this.padNr(now.getMinutes()),
        s: this.padNr(now.getSeconds())
      };
    });
  };

  padNr = nr => {
    return nr.toString().padStart(2, '0');
  };

  render = () => {
    return (
      <div className="Clock">
        {this.state.h}:{this.state.m}:{this.state.s}
      </div>
    );
  };
}

export default Clock;

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import './Link.css';

class Link extends React.Component {
  render() {
    return (
      <div className="Link">
        <RaisedButton
          label={this.props.label}
          href={this.props.href}
          primary={true}
          style={{
            margin: 12
          }}
        />
      </div>
    );
  }
}

export default Link;

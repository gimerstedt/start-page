let state = {};
const props = {
  locale: 'sv-SE',
  alarmData: {
    options: [
      new Date(0, 0, 0, 7),
      new Date(0, 0, 0, 7, 15),
      new Date(0, 0, 0, 7, 30),
      new Date(0, 0, 0, 7, 45),
      new Date(0, 0, 0, 8),
      new Date(0, 0, 0, 8, 15),
      new Date(0, 0, 0, 8, 30),
      new Date(0, 0, 0, 5, 0)
    ]
  },
  linkData: [
    { label: 'yc', href: 'https://news.ycombinator.com/' },
    { label: 'news', href: 'https://news.google.com/' },
    { label: 'phoronix', href: 'https://phoronix.com/' },
    { label: 'r/linux', href: 'https://reddit.com/r/linux' },
    { label: 'tltv', href: 'https://goo.gl/YkEmwi' },
    { label: 'tl24', href: 'https://goo.gl/B8BpsJ' },
    { label: 'fb', href: 'https://fb.com/' }
  ]
},
  el = (element, options) => {
    let newElement = document.createElement(element);
    for (let option in options) {
      newElement[option] = options[option];
    }
    return newElement;
  },
  append = (element, parent) =>
    parent ? parent.appendChild(element) : document.body.appendChild(element),
  replaceInner = (tagName, content) =>
    document.body.getElementsByTagName(tagName)[0].innerHTML = content,
  padNum = nr => nr.toString().padStart(2, '0'),
  msToFormattedSeconds = ms =>
    new Intl.NumberFormat(props.locale).format(Math.floor(ms / 1000)),
  createLink = ({ label, href }) => el('a', { href: href, innerHTML: label }),
  addClock = tagName => {
    state.clockElement = append(el(tagName));
    refreshClock();
    state.clockInterval = setInterval(refreshClock, 1000);
  },
  refreshClock = () => {
    const now = new Date(),
      dateTime = now.toLocaleString(props.locale),
      timestamp = msToFormattedSeconds(now.getTime());
    state.clockElement.innerHTML = `${timestamp}<br/>${dateTime}`;
  },
  addAlarm = tagName => {
    state.alarmElement = append(el(tagName));
    populateAlarm();
    state.alarmInterval = setInterval(soundAlarm, 1000);
  },
  populateAlarm = () => {
    const select = append(el('select'), state.alarmElement);
    props.alarmData.options.map(val =>
      append(
        el('option', {
          value: val,
          label: new Intl.DateTimeFormat(props.locale, {
            hour: 'numeric',
            minute: 'numeric'
          }).format(val)
        }),
        select
      ));
  },
  soundAlarm = () => {
    const now = new Date(),
      alarmTime = new Date(state.alarmElement.childNodes[0].value);
    if (
      now.getHours() === alarmTime.getHours() &&
      now.getMinutes() === alarmTime.getMinutes()
    ) {
      if (!state.alarming) {
        state.alarming = true;
        state.alarmSound = append(
          el('audio', {
            src: 'static/alarm.mp3',
            loop: true,
            autoplay: true
          })
        );
      }
    } else if (state.alarming) {
      state.alarming = false;
      document.body.removeChild(state.alarmSound);
    }
  },
  init = () => {
    const linkContainer = append(el('link-container'));
    props.linkData.map(item => append(createLink(item), linkContainer));
    addClock('the-clock');
    addAlarm('the-alarm');
  };

window.onload = () => init();
